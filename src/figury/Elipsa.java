package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;


public class Elipsa extends Figura{
	
	private static final long serialVersionUID = 1L;

	public Elipsa(Graphics2D buf, int del, int w, int h, boolean rounded) {
		super(buf, del, w, h, rounded);
		
		if(rounded) {
			shape = new Ellipse2D.Double(0.0,0.0,10.0,10.0);
		} else {
			shape = new Ellipse2D.Double(0.0,10.0,curvature(),curvature());
		}
		aft = new AffineTransform();
		area = new Area(shape);
	}

}

