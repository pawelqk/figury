package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

public class Kwadrat extends Figura{
	
	private static final long serialVersionUID = 1L;

	public Kwadrat(Graphics2D buf, int del, int w, int h, boolean rounded) {
		super(buf, del, w, h, rounded);
		if(isRectangle())
		{
			if(rounded) {
				shape = new RoundRectangle2D.Double(10,10,widthDifference(),10,curvature(),curvature());
			} else {
				shape = new Rectangle2D.Double(10.0,10.0,widthDifference(),widthDifference());
			}
		} else {
			if(rounded) {
				shape = new RoundRectangle2D.Double(10,10,10,10,curvature(),curvature());
			} else {
				shape = new Rectangle2D.Double(10.0,10.0,10.0,10.0);
			}
		}
		aft = new AffineTransform();
		area = new Area(shape);
	}
}
