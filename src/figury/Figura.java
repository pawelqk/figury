package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;

import javax.swing.JPanel;

/**
 * @author tb
 *
 */
public abstract class Figura extends JPanel implements Runnable, ActionListener {
	private static final long serialVersionUID = 1L;
	
	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	static private int width;
	static private int height;
	private Color clr;

	protected static final Random rand = new Random();

	public Figura(Graphics2D buf, int del, int w, int h, boolean rounded) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;

		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();
		
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
	}

	@Override
	public void run() {
		aft.translate(100, 100);
		area.transform(aft);
		shape = area;
		while (true) {
			if(AnimPanel.timer.isRunning())
				shape = nextFrame();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}
//
	protected Shape nextFrame() {
		area = new Area(area);
		aft = new AffineTransform();
		Rectangle bounds = area.getBounds();
		int cx = bounds.x + bounds.width / 2;
		int cy = bounds.y + bounds.height / 2;
	
		if (bounds.x <= 5 || bounds.x + bounds.width >= width - 5) {
			dx = -dx;
			}
		if (bounds.y <= 5 || bounds.y + bounds.height >= height - 5) {
			dy = -dy;
			}
					
		
		if (bounds.height >= height / 3 || bounds.height <= 10)
			sf = 1 / sf;
		
		aft.translate(cx, cy);
		aft.scale(sf, sf);
		aft.rotate(an);
		aft.translate(-cx, -cy);
		aft.translate(dx, dy);
		
		area.transform(aft);
		return area;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		buffer.setColor(clr.darker());
		buffer.draw(shape);
	}
	
	public int curvature(){
		return 5+rand.nextInt(15);
	}
	
	public boolean isRectangle() {
		return rand.nextBoolean();
	}
	
	public int widthDifference() {
		return rand.nextInt(20);
	}
}